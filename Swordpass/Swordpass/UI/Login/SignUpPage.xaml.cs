﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Swordpass
{
	public partial class SignUpPage : ContentPage
	{

		private SwordpassDatabase db = new SwordpassDatabase();
		public SignUpPage ()
		{
			InitializeComponent ();

			signupButton.Clicked += OnSignUpButtonClicked;
			Title = "Sign Up";

			//focus pass
			usernameEntry.Completed += (s,e) => passwordEntry.Focus ();
			passwordEntry.Completed += (s,e) => password2Entry.Focus ();


		}

		//username + password + password2 can't be null and can't have 0 length
		//password has to equal password2
		private bool checkValidity(string username, string password, string password2)
		{
			if (username == null || password == null || password2 == null) {
				return false;
			}

			if (username.Length == 0 || password.Length == 0 || password2.Length == 0) {
				return false;
			}
			if (password != password2) {
				return false;
			}
				
			return true;
		}

		 void OnSignUpButtonClicked(object sender, EventArgs e) {


			var username = usernameEntry.Text;
			var password = passwordEntry.Text;
			var password2 = password2Entry.Text;
		

			if (checkValidity(username, password, password2)) {
				//save user in db and set security context
				User user = db.SaveUser (username, password);

				SecurityContext.GetInstance ().LoadUser(password, user);

		
			 App.Current.MainPage = new MainPage();
				 


			} else {
				messageLabel.Text = "Sign  Up failed";
				passwordEntry.Text = string.Empty;
				usernameEntry.Text = string.Empty;
				password2Entry.Text = string.Empty;

			}


		}
	}
}

