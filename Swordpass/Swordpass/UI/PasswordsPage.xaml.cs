﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Collections.ObjectModel;


namespace Swordpass
{
	public partial class PasswordsPage : ContentPage
	{
		private SwordpassDatabase db = new SwordpassDatabase();
		private ObservableCollection<Password> passwordList;

		public PasswordsPage ()
		{
			InitializeComponent ();
			Title = "Passwords";
			//add passwort action bar item
			var toolbarItem = new ToolbarItem("Add", "plus_50.png", () =>
				{

					StartAddPasswordPage();			

				}, 0, 0);
			ToolbarItems.Add(toolbarItem);

			passwordList = new ObservableCollection<Password>();

			//get password list from  db
			var passwordListDb = db.GetPasswordsForUser (SecurityContext.GetInstance ().GetUser ().ID);

			foreach (var item in passwordListDb) {
				passwordList.Add (item);

			}



			//set list view properties
			passwordListView.RowHeight = 80;
			passwordListView.ItemTemplate = new DataTemplate (typeof(CustomPasswordCell));
			passwordListView.ItemsSource = passwordList;

			passwordListView.ItemSelected += (sender, e) => {


				OnItemClicked((Password)e.SelectedItem);
			};
		}

		async void OnItemClicked (Password pass)
		{
			var answer = await DisplayAlert ("Delete?", "Would you like to delete the Password", "Delete", "Cancel");
			if (answer) {
				db.DeletePassword (pass.ID);
				passwordList.Remove (pass);

			}

		}

		private  void StartAddPasswordPage()	{
			App.Current.MainPage = new NavigationPage(new AddPasswordPage());
		}

		public class CustomPasswordCell : ViewCell
		{
			//color roulette to choose random color
			private Color[] cellColors = {Color.FromHex("#118EED"), Color.FromHex("#f44336"), Color.FromHex("#673AB7"), Color.FromHex("#FF5722"), Color.FromHex("#8BC34A")};

			public CustomPasswordCell()
			{
				
				AbsoluteLayout cellView = new AbsoluteLayout (){ BackgroundColor= Color.White};
				//view for url
				var urlLabel = new Label ();
				urlLabel.SetBinding (Label.TextProperty, new Binding ("Url"));
				AbsoluteLayout.SetLayoutBounds (urlLabel,
					new Rectangle(100, 10, 400, 40));
				urlLabel.FontSize = 24;
				cellView.Children.Add (urlLabel);

				//view for username
				var userLabel = new Label ();
				userLabel.SetBinding (Label.TextProperty, new Binding ("UserName"));
				AbsoluteLayout.SetLayoutBounds (userLabel,
					new Rectangle(100, 40, 200, 25));
				cellView.Children.Add (userLabel);

				//creates the colored rectangle and assigns random color
				BoxView boxView = new BoxView();
				Random random = new Random();
				int randomColor = random.Next(0, cellColors.Length);
				boxView.Color = cellColors[randomColor];
				AbsoluteLayout.SetLayoutBounds (boxView,
					new Rectangle(30, 15, 50, 50));
				cellView.Children.Add (boxView);


			    //create the big upper case letter
				var letterLabel = new Label ();
				letterLabel.FontSize = 45;
				letterLabel.SetBinding (Label.TextProperty, new Binding ("Url", BindingMode.Default, new LetterConverter(), null));
				letterLabel.TextColor = Color.White;
				AbsoluteLayout.SetLayoutBounds (letterLabel,
					new Rectangle(42.5, 10, 70, 70));
				cellView.Children.Add(letterLabel);



				this.View = cellView;
			}

			//binding converter to turn the url string to the first letter uppercase
			public class LetterConverter : IValueConverter
			{
				#region IValueConverter implementation

				public object Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
				{
					
					return ((string) value).ToUpper().Substring(0,1);
				}

				public object ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
				{
					throw new NotImplementedException ();
				}

				#endregion

			}

		}
	}
}

