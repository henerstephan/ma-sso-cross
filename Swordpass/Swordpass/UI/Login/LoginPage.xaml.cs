﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Swordpass
{
	public partial class LoginPage : ContentPage
	{

		private SwordpassDatabase db = new SwordpassDatabase();
		public LoginPage ()
		{
			InitializeComponent ();
			var toolbarItem = new ToolbarItem {
				Text = "Sign Up"
			};
			toolbarItem.Clicked += OnSignUpButtonClicked;
			ToolbarItems.Add(toolbarItem);
			loginButton.Clicked += OnLoginButtonClicked;

			//focus pass
			usernameEntry.Completed += (s,e) => passwordEntry.Focus ();

			Title = "Login";
		}

		async void OnSignUpButtonClicked(object sender, EventArgs e) {
			await Navigation.PushAsync(new SignUpPage());
		}

		//username + password can't be null and have 0 lenght
		private bool CheckValidity(string username, string password) {
			if(username == null || password == null) {
				return false;
			}

			if (username.Length == 0 || password.Length == 0) {
				return false;
			}
			return true;

		}

		private void LoginFailed()
		{
			messageLabel.Text = "Login failed";
			passwordEntry.Text = string.Empty;
			usernameEntry.Text = string.Empty;
		}

		 void OnLoginButtonClicked(object sender, EventArgs e) {


			string username = usernameEntry.Text;
			string plainpass = passwordEntry.Text;



			if(!CheckValidity(username, plainpass)){
					LoginFailed();
					return;
				}




			//if user is found login and set security context
			User user = db.FindUser (username, plainpass);
			if (user != null) {
				SecurityContext.GetInstance ().LoadUser (plainpass, user);
				App.Current.MainPage = (new MainPage ());


			} else {

				LoginFailed ();
			}





			}

	
		}

}

