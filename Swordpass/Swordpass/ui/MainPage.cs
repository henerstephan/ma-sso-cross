using System;
using Xamarin.Forms;


namespace Swordpass
{
	class MainPage : TabbedPage
	{

		public MainPage()
		{
			//creates main screen
			var passwordsPage = new NavigationPage (new PasswordsPage());
			passwordsPage.Icon = "key.png";
			passwordsPage.Title = "Passwords";

			var bondsPage = new NavigationPage (new BondsPage ());

			bondsPage.Icon = "chrome.png";
			bondsPage.Title = "Bonds";


			Children.Add (passwordsPage);
			Children.Add (bondsPage);
		}


	}

}

