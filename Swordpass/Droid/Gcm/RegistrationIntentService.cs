﻿using System;
using Android.App;
using Android.Content;
using Android.Util;
using Android.Gms.Gcm;
using Android.Gms.Gcm.Iid;

namespace Swordpass.Droid
{
	[Service(Exported = false)]	
	public class RegistrationIntentService : IntentService
	{
		static object locker = new object();

		public RegistrationIntentService() : base("RegistrationIntentService") { }

		protected override void OnHandleIntent (Intent intent)
		{
			try
			{
				Log.Info ("RegistrationIntentService", "Calling InstanceID.GetToken");
				lock (locker)
				{
					var instanceID = InstanceID.GetInstance (Android.App.Application.Context);
					var token = instanceID.GetToken (
						"905584943881", GoogleCloudMessaging.InstanceIdScope, null);

					Log.Info ("RegistrationIntentService", "GCM Registration Token: " + token);
					SendRegistrationToAppServer (token);
					Subscribe (token);
				}
			}
			catch (Exception e)
			{
				Log.Debug("RegistrationIntentService", "Failed to get a registration token");
				return;
			}
		}

		void SendRegistrationToAppServer (string token)
		{
			SecurityContext.GetInstance ().SetToken (token);
		}

		void Subscribe (string token)
		{
			var pubSub = GcmPubSub.GetInstance(Android.App.Application.Context);
			pubSub.Subscribe(token, "/topics/global", null);
		}
		
	}
}

