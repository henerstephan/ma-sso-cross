﻿using System;

namespace Swordpass
{
	//class holds information about the current user
	//set after login + signup via the loadUser() function
	//also contains the push token
	public class SecurityContext
	{

		private SwordpassDatabase db = new SwordpassDatabase();

		private static SecurityContext instance = new SecurityContext();

		private string pushToken;

		private SecurityContext () {
		}

		public User GetUser () {
			return user;
		}

		public void SetToken (string token)
		{
			pushToken = token;
		}

		public string GetToken()
		{
			return pushToken;
		}

		private byte[] encryptionKey;
		private Key encryptedKey;
		private User user;

		public static SecurityContext GetInstance()
		{
			return instance;
		}

		public void LoadUser(String plainPassword, User user)
		{
			//load user data
			this.user = user;
			encryptedKey = db.GetKey (user.ID);
			this.user.Salt = db.GetSalt (user.ID);
		    encryptionKey = Crypto.DecryptAES (encryptedKey.UserKey, Crypto.GenerateAESKey (plainPassword, this.user.Salt), encryptedKey.IV);
		}

		//get the master encryption key
		public byte[] GetPasswordEncryptionKey()
		{

			return Crypto.DecryptAES (encryptedKey.UserKey, encryptionKey, encryptedKey.IV);
		}
	}
}

