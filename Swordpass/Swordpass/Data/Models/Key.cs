﻿using System;
using SQLite;

namespace Swordpass
{
	public class Key
	{
		public Key ()
		{
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
	


		public byte[] UserKey { get; set; }

		public byte[] IV { get; set; }


		public long UserId {get; set;}
	}
}

