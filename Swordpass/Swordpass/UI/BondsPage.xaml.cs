﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace Swordpass
{
	public partial class BondsPage : ContentPage
	{

		private SwordpassDatabase db = new SwordpassDatabase();
		private ObservableCollection<Bond> bondsList;
		public BondsPage ()
		{
			InitializeComponent ();

			Title = "Bonds";
			var toolbarItem = new ToolbarItem("Add", "plus_50.png", () =>
				{

					StartQRCodeReader();	
					//TestBonds();

				}, 0, 0);
			ToolbarItems.Add(toolbarItem);

			//loads bond from database
			//observable collection to let list auto update
			bondsList = new ObservableCollection<Bond>();
			var bondsListDb = db.GetAllBonds(SecurityContext.GetInstance ().GetUser ().ID);

			foreach (var item in bondsListDb) {
				bondsList.Add (item);

			}

			//set list info
			bondsListView.RowHeight = 60;
			bondsListView.ItemTemplate = new DataTemplate (typeof(CustomBondsCell));
			bondsListView.ItemsSource = bondsList;

			bondsListView.ItemSelected += (sender, e) => {


				OnItemClicked((Bond)e.SelectedItem);
			};
		}

		async void OnItemClicked (Bond bond)
		{
			var answer = await DisplayAlert ("Delete?", "Would you like to delete the Bond", "Delete", "Cancel");
			if (answer) {
				db.DeleteBond (bond.ID);
				bondsList.Remove (bond);
			}

		}
	

		private async void StartQRCodeReader() {




			var scanner = new ZXing.Mobile.MobileBarcodeScanner();
			var qrCodeResult = await scanner.Scan();

			if (qrCodeResult != null) {

				//qr code hast form: <bond_id>#<aes_key>#<aes_iv>

				string[] result = qrCodeResult.ToString ().Split ('#');

				//create the establish bond request, set name, push id
				EstablishBondRequest establishBondRequest = new EstablishBondRequest ();
				string bondId = result [0];
				establishBondRequest.push_id = SecurityContext.GetInstance ().GetToken ();
				establishBondRequest.name = "chrome";

				//get iv and the key
				byte[] iv = Base64.Decode(result [2]);
				byte[] keyRaw = Base64.Decode (result [1]);
				//encrypt the raw key with the master encryption key
				byte[] key = Crypto.EncryptAES (keyRaw, SecurityContext.GetInstance ().GetPasswordEncryptionKey (), iv);

				//generate user data
				establishBondRequest.user_info = LoginDataGenerator.GetUserData (keyRaw, iv);

				//send to server
				await RestApi.getInstance ().EstablishBondRequest (bondId, establishBondRequest);


				//create bond object in database, add to list
				Bond bond = new Bond();
				bond.AESIv = iv;
				bond.AESKey = key;
				bond.BondName = establishBondRequest.name;
				bond.BondId = bondId;
				bond.UserId = SecurityContext.GetInstance ().GetUser ().ID;
				bondsList.Add(bond);
				new SwordpassDatabase().SaveBond(bond);


			}
		}

	

		public class CustomBondsCell : ViewCell
		{
			public CustomBondsCell()
			{
				AbsoluteLayout cellView = new AbsoluteLayout (){ BackgroundColor= Color.White};

				//view for the bond name
				var typeLabel = new Label ();
				typeLabel.SetBinding (Label.TextProperty, new Binding ("BondName"));
				AbsoluteLayout.SetLayoutBounds (typeLabel,
					new Rectangle(100, 10, 300, 50));
				cellView.Children.Add (typeLabel);
				typeLabel.FontSize = 30;

				//view for the image
				var image = new Image ();
				image.Source = "chrome_color.png";
				AbsoluteLayout.SetLayoutBounds (image,
					new Rectangle(30, 5, 50, 50));
				cellView.Children.Add (image);

				this.View = cellView;
			}

		}
	}
}

