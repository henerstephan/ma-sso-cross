﻿using System;
using SQLite;

namespace Swordpass
{
	public class User
	{

		public User(){}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }

		public string Name { get; set; }

		public byte[] Password { get; set; }

		public byte[] Salt { get; set; }

	}
}

