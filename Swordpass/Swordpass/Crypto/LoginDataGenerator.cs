﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;

namespace Swordpass
{
	public class LoginDataGenerator
	{


		//generates the data for a login requests
		public static SendPasswordData GenerateSendPasswordRequest(LoginRequest request)
		{
			SwordpassDatabase db = new SwordpassDatabase ();

			//get master encrytion key and password
			byte[] masterEncryptionKey = SecurityContext.GetInstance ().GetPasswordEncryptionKey ();
			Password password = db.FindPassword (request.Url, request.UserName);
		
			SendPasswordData data = new SendPasswordData ();
			data.SlashId = request.SlashId;

			//get bond information
			Bond bond = db.GetBond (request.BondId);
		
			//decrypt the bond encryption key with the master encryption key
			byte[] bondKey = Crypto.DecryptAES (bond.AESKey, masterEncryptionKey, bond.AESIv);

			//decrypt the password
			byte[] plainPass = Crypto.DecryptAES (password.UserPassword, masterEncryptionKey, password.IV);

			//encrypt password with bond encrypt key and creat the send password request object
			SendPasswordRequest pwRequest = new SendPasswordRequest(Base64.Encode (Crypto.EncryptAES (plainPass, bondKey, bond.AESIv)));
			data.PasswordRequest = pwRequest;

			return data;
		}

		//generates the user data only for the inital bond 
		//no password are sent and the data is encrypted by the encryption key from the bond
		public static string GetUserData (byte[] keyRaw, byte[] iv)
		{
			//get a list of all password
			List<Password> passwordList = new List<Password>();
			passwordList.AddRange(new SwordpassDatabase ().GetPasswordsForUser (SecurityContext.GetInstance ().GetUser ().ID));

			//create a list of user data (username + url)
			List<UserData> userDataList = new List<UserData> ();
			foreach (var pw in passwordList) {
				UserData data = new UserData ();
				data.name = pw.UserName;
				data.url = pw.Url;
				userDataList.Add (data);
			}		

			//turn into json
			string rawUserData = JsonConvert.SerializeObject (userDataList);

			//encrypt it with bond encryption key
			byte[] encryptedPass = Crypto.EncryptAES (Encoding.UTF8.GetBytes(rawUserData), keyRaw, iv);

			//base64 encode and return
			return Base64.Encode(encryptedPass);
		}

		public  class SendPasswordData {

			public string SlashId;
			public SendPasswordRequest PasswordRequest;
		}

		public  class UserData {

			public string name;
			public string url;
		}
	}
}

