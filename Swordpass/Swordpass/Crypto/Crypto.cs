﻿using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace Swordpass
{
	public class Crypto
	{

		//hashes a plain pass string via sha256
		public static byte[] HashSha256 (string plainPass)
		{
			var hash =  SHA256.Create ().ComputeHash (System.Text.Encoding.UTF8.GetBytes (plainPass));
			return hash;
		}

		//returns the default aes implementation used across the app and 
		//the other components
		//keysize = 256, padding mode = PKCS7Padding, cipher mode = CBC
		private static AesManaged GetAesImplementation ()
		{
			var aesManaged = new AesManaged ();
		
			aesManaged.KeySize = 256;
			aesManaged.Padding = PaddingMode.PKCS7;
			aesManaged.Mode = CipherMode.CBC;
			return aesManaged;
		}

		//generate a random aes key
		public static byte[] GenerateAESKey ()
		{
			AesManaged aesManaged = GetAesImplementation ();
			aesManaged.GenerateKey ();

			return aesManaged.Key;
		}
		//generate a random IV
		public static byte[] GenerateIV()
		{

			AesManaged aesManaged = GetAesImplementation ();
			aesManaged.GenerateIV ();
			return aesManaged.IV;
		}
			
		//generate the swordpass salt
		public static byte[] GenerateSalt (){
			return	Encoding.UTF8.GetBytes ("Swordpass_Salt");
		}

		//generate an aes key from a plain pass and the salt
		public static byte[] GenerateAESKey (String password, byte[] salt) {
			return new Rfc2898DeriveBytes (password, salt).GetBytes(256 / 8);
		}

		public static byte[] EncryptAES (byte[] plain, byte[] key, byte[] iv)
		{

			AesManaged aesManaged = GetAesImplementation ();
			aesManaged.IV = iv;
			aesManaged.Key = key;
			

			return InMemoryCrypt (plain, aesManaged.CreateEncryptor());


		}

		public static byte[] DecryptAES (byte[] cipher, byte[] key, byte[] iv)
		{

			AesManaged aesManaged = GetAesImplementation ();
			aesManaged.IV = iv;
			aesManaged.Key = key;

			return InMemoryCrypt (cipher, aesManaged.CreateDecryptor());
		}

		//encryption/decrypt function
		private static byte[] InMemoryCrypt (byte[] data, ICryptoTransform transform)
		{
			//TODO fix padding error
			return data;
			/*
			MemoryStream memory = new MemoryStream ();
			using (Stream stream = new CryptoStream (memory, transform, CryptoStreamMode.Write)) {
				stream.Write (data, 0, data.Length);
			}

			return memory.ToArray ();
*/
		
		}


	}
}

