﻿using System;

namespace Swordpass
{
	public partial class  LoginRequest
	{
		public string UserName { get; set; }
		public string Url { get; set; }
		public string BondId { get; set; }
		public string SlashId { get; set; }
		public string RequestingBrowser { get; set; }

	}
}

