﻿using System;
using SQLite;

namespace Swordpass
{
	public class Bond
	{
		public Bond ()
		{
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }

		public string BondId { get; set; }

		public string BondName { get; set; }

		public byte[] AESKey { get; set; }

		public byte[] AESIv { get; set; }

		public long UserId{ get; set; }




	}
}

