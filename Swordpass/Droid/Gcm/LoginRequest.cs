﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Util;

namespace Swordpass
{
	public partial class LoginRequest 
	{
		


		public Bundle ToBundle()
		{
			Bundle b = new Bundle ();
			b.PutString ("UserName", UserName);
			b.PutString ("Url", Url);
			b.PutString ("BondId", BondId);
			b.PutString ("SlashId", SlashId);
			b.PutString ("RequestingBrowser", RequestingBrowser);
			return b;
		}


		public static LoginRequest FromBundle(Bundle bundle) {
			LoginRequest request = new LoginRequest ();
			request.UserName = bundle.GetString ("UserName");
			request.Url = bundle.GetString ("Url");
			request.BondId = bundle.GetString ("BondId");
			request.SlashId = bundle.GetString ("SlashId");
			request.RequestingBrowser = bundle.GetString ("RequestingBrowser");
			return request;

		}

	}



}


