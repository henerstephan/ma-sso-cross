﻿using System;
using SQLite;


namespace Swordpass
{
	public class Password
	{
		public Password ()
		{
		}


		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }

		public string Url { get; set; }

		public string UserName { get; set; }


		public byte[] UserPassword { get; set; }

		public byte[] IV { get; set; }


		public long UserId {get; set;}


	}
}

