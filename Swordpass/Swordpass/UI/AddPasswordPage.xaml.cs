﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;

namespace Swordpass
{
	public partial class AddPasswordPage : ContentPage
	{

		private SwordpassDatabase db = new SwordpassDatabase ();

		public AddPasswordPage ()
		{
			InitializeComponent ();
			Title = "Add Password";

			cancelButton.Clicked += cancel;
			//focus pass
			saveButton.Clicked += savePassword;
			urlEntry.Completed += (s,e) => usernameEntry.Focus ();
			usernameEntry.Completed += (s,e) => passwordEntry.Focus ();

		}

		void cancel (object sender, EventArgs e)
		{

			closePage ();
		}


		private void closePage ()
		{

			App.Current.MainPage = new MainPage ();

		}


		//can't be null and length can't be 0
		private bool validate (string username, string url, string password)
		{

			if (username == null || password == null || url == null) {
				return false;
			}
			if (username.Length == 0 || url.Length == 0 || password.Length == 0) {
				return false;
			}
			return true;
		}

		void savePassword (object sender, EventArgs e)
		{

			messageLabel.Text = "";
			string url = urlEntry.Text;
			string username = usernameEntry.Text;
			string passwordInput = passwordEntry.Text;
			if (validate (username, url, passwordInput)) {

				//set password data and generate iv
				Password pass = new Password ();
				pass.Url = urlEntry.Text;
				pass.UserName = usernameEntry.Text;
				pass.IV = Crypto.GenerateIV ();
				pass.UserId = SecurityContext.GetInstance ().GetUser ().ID;

				//get master encryption key, encrypt password with it and save it to database
				byte[] key = SecurityContext.GetInstance ().GetPasswordEncryptionKey ();
				pass.UserPassword = Crypto.EncryptAES (Encoding.ASCII.GetBytes (passwordEntry.Text), key, pass.IV) ;
				db.SavePassword (pass);

				closePage ();
			} else {
				messageLabel.Text = "Invalid Input";
			}



		

		}
	}
}

