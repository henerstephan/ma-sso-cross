﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web;


namespace Swordpass
{
	public class RestApi
	{
		//private const string BaseUrl = "http://swordpass.heroku.com/";
		private const string BaseUrl = 		"http://192.168.1.120:3000/";

	

		private const string Bond =  "bonds/";

		private const string Slash =  "slahes/";

		private static RestApi instance = new RestApi();

		private HttpClient client;

		public RestApi ()
		{
			client = new System.Net.Http.HttpClient ();
			client.MaxResponseContentBufferSize = 256000;

		}


		public static RestApi getInstance()
		{
			return instance;
		}


		public async Task<NamedResponse> EstablishBondRequest (string bondId, EstablishBondRequest bondRequest)
		{
			var uri = new Uri (RestApi.BaseUrl + RestApi.Bond + HttpUtility.UrlEncode(bondId));

			var json = JsonConvert.SerializeObject (bondRequest);
			var content = new StringContent (json, Encoding.UTF8, "application/json");

			HttpResponseMessage response = await client.PutAsync (uri, content);



			if (response.IsSuccessStatusCode) {
				var jsonResponse = await response.Content.ReadAsStringAsync ();

				return JsonConvert.DeserializeObject <NamedResponse> (jsonResponse);
			}

			return null;
		}


		public async Task<Boolean> sendPassword (string slashId, SendPasswordRequest sendPasswordRequest)
		{

			var uri = new Uri (RestApi.BaseUrl + RestApi.Slash + HttpUtility.UrlEncode(slashId));

			var json = JsonConvert.SerializeObject (sendPasswordRequest);
			var content = new StringContent (json, Encoding.UTF8, "application/json");

			HttpResponseMessage response = await client.PutAsync (uri, content);


			if (response.IsSuccessStatusCode) {

				return true;
			}

			return false;

		}

		public async Task<Boolean> deleteBond (string bondId)
		{

			var uri = new Uri (RestApi.BaseUrl + RestApi.Bond + HttpUtility.UrlEncode((bondId)));

			HttpResponseMessage response = await client.DeleteAsync (uri);

			if (response.IsSuccessStatusCode) {

				return true;
			}

			return false;

		}




	

	}


}

