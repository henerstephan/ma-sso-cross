﻿using System;

namespace Swordpass
{
	public class SendPasswordRequest
	{

		public string encrypted_password;

		public SendPasswordRequest (string encryptedPassword)
		{
			encrypted_password = encryptedPassword;
		}
	}
}

