﻿using System;

namespace Swordpass
{
	public class DatabaseService
	{
		private static DatabaseService instance;

		private DatabaseService ()
		{
		}

		public static DatabaseService Instance {
			get {
				if (instance == null) {
					instance = new DatabaseService ();
				}
				return instance;
			}
		}


	}
}

