﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Gms.Gcm;
using Android.Util;

namespace Swordpass.Droid
{

	[Service (Exported = false), IntentFilter (new [] { "com.google.android.c2dm.intent.RECEIVE" })]
	public class SwordpassGcmListenerServiceGcmListenerService : GcmListenerService
	{

		private const string SEND_PASSWORD = "send_password";

		public override void OnMessageReceived (string from, Bundle data)
		{
			
			LoginRequest loginRequest = new LoginRequest ();

			loginRequest.UserName = data.GetString ("username");
			loginRequest.Url = data.GetString ("url");
			loginRequest.BondId = data.GetString ("bond_id");
			loginRequest.SlashId = data.GetString ("slash_id");
			loginRequest.RequestingBrowser = "chrome";


			SendNotification (loginRequest);
		
		}

		void SendNotification(string message)
		{

			var intent = new Intent (this, typeof(MainActivity));
			intent.AddFlags (ActivityFlags.ClearTop);
			var pendingIntent = PendingIntent.GetActivity (this, 0, intent, PendingIntentFlags.OneShot);

			var notificationBuilder = new Notification.Builder(this)
				.SetSmallIcon (Resource.Drawable.icon)
				.SetContentTitle ("GCM Message")
				.SetContentText (message)
				.SetAutoCancel (true)
				.SetContentIntent (pendingIntent);

			var notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
			notificationManager.Notify (0, notificationBuilder.Build());
		}

		void SendNotification (LoginRequest request)
		{
			var intent = new Intent (this, typeof(NotificationActionService));
			var pendingIntent = PendingIntent.GetService (this, 0, intent, PendingIntentFlags.OneShot);
			intent.PutExtra (SEND_PASSWORD, request.ToBundle ());

			var notificationBuilder = new Notification.Builder(this)
				.SetSmallIcon (Resource.Drawable.icon)
				.SetContentTitle ("LoginRequest")
				.SetContentText (request.RequestingBrowser + ": " + request.UserName + "@" + request.Url + ". Allow?") 
				.SetAutoCancel (true)
				.SetContentIntent (pendingIntent);
			

			var notificationManager = (NotificationManager) GetSystemService(Context.NotificationService);
			notificationManager.Notify (0, notificationBuilder.Build());
		}

		public class NotificationActionService : IntentService {



			protected async override void OnHandleIntent(Intent intent)
			{
				LoginRequest loginRequest = LoginRequest.FromBundle(intent.GetBundleExtra(SEND_PASSWORD));
					
				LoginDataGenerator.SendPasswordData sendPasswordRequest = LoginDataGenerator.GenerateSendPasswordRequest (loginRequest);
				await RestApi.getInstance ().sendPassword (sendPasswordRequest.SlashId, sendPasswordRequest.PasswordRequest);

		
			}

		}
	}
}

