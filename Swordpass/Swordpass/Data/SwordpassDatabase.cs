﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SQLite;


namespace Swordpass
{
	public class SwordpassDatabase
	{
		static object locker = new object ();

		SQLiteConnection database;

		string DatabasePath {
			get { 
				var sqliteFilename = "Swordpass.db3";
				#if __IOS__
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
				var path = Path.Combine (libraryPath, sqliteFilename);
				#else
				#if __ANDROID__
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				var path = Path.Combine(documentsPath, sqliteFilename);
				#else
				// WinPhone
				var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, sqliteFilename);;
				#endif
				#endif
				return path;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Tasky.DL.TaskDatabase"/> TaskDatabase. 
		/// if the database doesn't exist, it will create the database and all the tables.
		/// </summary>
		/// <param name='path'>
		/// Path.
		/// </param>
		public SwordpassDatabase ()
		{
			database = new SQLiteConnection (DatabasePath);
			// create the tables
			database.CreateTable<Bond> ();
			database.CreateTable<Key> ();
			database.CreateTable<Password> ();
			database.CreateTable<User> ();



		}

		public IEnumerable<Bond> GetAllBonds (int userId)
		{
			lock (locker) {
				return database.Query<Bond> ("SELECT * FROM Bond WHERE UserId = " + userId);

			}
		}

	
		public IEnumerable<Password> GetPasswordsForUser (int userId)
		{
			lock (locker) {
				return database.Query<Password> ("SELECT * FROM Password WHERE UserId = " + userId);
			}
		}

		public Bond GetBond (string bondId)
		{
			lock (locker) {
				return database.Table<Bond> ().FirstOrDefault (x => x.BondId == bondId);
			}
		}

		public Key GetKey (int userId)
		{
			lock (locker) {
				return database.Table<Key> ().FirstOrDefault (x => x.UserId == userId);
			}
		}

		public byte[] GetSalt (int userId)
		{
			lock (locker) {
				return database.Table<User> ().FirstOrDefault (x => x.ID == userId).Salt;
			}		
		}

		public int SavePassword (Password item)
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update (item);
					return item.ID;
				} else {
					return database.Insert (item);
				}
			}
		}


		public Password FindPassword (string url, string username)
		{
			lock (locker) {
				List<Password> password = database.Query<Password> ("SELECT * FROM [Password] WHERE [UserName] = " + username + " AND [Url] = " + url).ToList ();
				if (password.Count != 0) {

					return password.ElementAt (0);
				} else {
					return null;
				}
			}
		}

		public User FindUser (string username, string plainpass)
		{
			lock (locker) {
				List<User> users = database.Query<User> ("SELECT * FROM User WHERE Name = \"" + username  + "\""  ).ToList ();
				if (users.Count != 0) {
					User user = users.ElementAt (0);
					if (user.Password.SequenceEqual (Crypto.HashSha256 (plainpass))) {
						return user;
					} else {
						return null;
					}
				} else {
					return null;
				}
			}
		}

		public int SaveKey (long userId, byte[] encryptionKey)
		{

			byte[] key = Crypto.GenerateAESKey (); 
			byte[] iv = Crypto.GenerateIV ();
			byte[] encryptedKey = Crypto.EncryptAES (key, encryptionKey, iv); 
			Key item = new Key ();
			item.UserKey = encryptedKey;
			item.UserId = userId;
			item.IV = iv;

			lock (locker) {
				if (item.ID != 0) {
					database.Update (item);
					return item.ID;
				} else {
					return database.Insert (item);
				}
			}

		}

		public User SaveUser (string username, string password)
		{


			lock (locker) {

				User item = new User ();
				item.Name = username;
				item.Password = Crypto.HashSha256 (password);
				item.Salt = Crypto.GenerateSalt ();


				item.ID = database.Insert (item);


				SaveKey (item.ID, Crypto.GenerateAESKey (password, item.Salt));

				return item;
			}
		}

		public int SaveBond (Bond item)
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update (item);
					return item.ID;
				} else {
					return database.Insert (item);
				}
			}
		}

		public int DeleteBond (int id)
		{
			lock (locker) {
				return database.Delete<Bond> (id);
			}
		}

		public int DeletePassword (int id)
		{
			lock (locker) {
				return database.Delete<Password> (id);
			}		}
	}
}

